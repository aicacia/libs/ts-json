# ts-json

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-json/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/json)](https://www.npmjs.com/package/@aicacia/json)
[![pipelines](https://gitlab.com/aicacia/libs/ts-json/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-json/-/pipelines)

JSON types and helper functions
